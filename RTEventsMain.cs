﻿/**********************************************************
 * Demo for Standalone SDK.Created by Darcy on Oct.15 2009*
***********************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System;
using System.Net;
using System.Text;
using System.IO;
using Microsoft.Win32;
using System.Xml;
using System.Web.Script.Serialization;

using System;
using System.Net;
using System.Net.NetworkInformation;


namespace RTEvents
{
    public partial class RTEventsMain : Form
    {

        private DateTime _start, _tempStart;
        private static string url = "http://hras.warnawarni.co.id/api/";
      //  private static string url = "http://dev.mediainovasi.id/api/";
        string[] methodAbsen;
        string[] namaLokasi;
        string[] latitute;
        string[] longitude;
        private static int absolute = 9;

        //#0 ARJUNO
        //#1 BLORA 10
        //#2 BLORA 38
        //#3 CENTRAL MALL
        //#4 MANYAR
        //#5 MERR
        //#6 TIDAR
        //#7 PANGSUD
        //#8 PURI
        //#9 WONOREJO



        public RTEventsMain()
        {
            InitializeComponent();
           
            methodAbsen = new string[6]  { "IN", "OUT", "Break Out","Break In", "Lembur Masuk", "Lembur Keluar" };
           
            namaLokasi   = new string[10]  { "ARJUNO",        "BLORA 10",     "BLORA 38",     "CENTRAL MALL",     "MANYAR",       "MERR",         "TIDAR",        "PANGSUD",      "PURI",         "WONOREJO" };
            latitute     = new string[10]  { "-7.266671",     "-6.199976",    "-6.202163",    "-7.2547858",       "-7.289756",    "-7.311581",    "-7.253965",    "-7.265358",    "-6.179566",    "-7.267924" };
            longitude    = new string[10]  { "112.727368",    "106.823537",   "106.823380",   "112.7157564",      "112.768972",   "112.781684",   "112.715745",   "112.745537",   "106.748443",   "112.730857" };
           
            this.Text = "Absensi " + namaLokasi[absolute];
            txtLat.Text = latitute[absolute];
            txtLong.Text = longitude[absolute];


            //get address 
            try
            {
                System.Net.WebClient oWeb = new System.Net.WebClient();
                oWeb.Proxy = System.Net.WebRequest.DefaultWebProxy;
                oWeb.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string response = oWeb.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + txtLat.Text + "," + txtLong.Text + "&key=AIzaSyD88V4RF0YNpV0eGNeI_hH-EuX2mrQh7cQ");
                string temp = response;

                RootObjectAddress respon = new RootObjectAddress();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                respon = (RootObjectAddress)jss.Deserialize(temp, typeof(RootObjectAddress));

                if (respon.results.Count > 0)
                {
                    txtAlamat.Text = respon.results[0].formatted_address.ToString();
                }
            }
            catch (Exception e)
            {
                txtAlamat.Text = "";
            }


            //get jam server
            try
            {
                System.Net.WebClient oWeb = new System.Net.WebClient();
                oWeb.Proxy = System.Net.WebRequest.DefaultWebProxy;
                oWeb.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string response = oWeb.DownloadString(url+"getservertime");
                string temp = response;

                RootObjectJamServer respon = new RootObjectJamServer();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                respon = (RootObjectJamServer)jss.Deserialize(temp, typeof(RootObjectJamServer));

                _start = respon.data;
                trJamServer.Start();
                trReconnect.Start();
                

                label9.Text = respon.data.ToString("dddd,dd MMMM yyyy");
                lblStatus.Text = "Connected";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception e)
            {
                lblStatus.Text = "Not Connected";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

            //get maps on browser
            StringBuilder queryaddress = new StringBuilder();
            queryaddress.Append("https://maps.googleapis.com/maps/api/staticmap?center=" + txtLat.Text + "," + txtLong.Text + "&maptype=roadmap&zoom=18&size=496x387&markers=" + txtLat.Text + "," + txtLong.Text + "|" + txtLat.Text + "," + txtLong.Text + "&markers=color:blue%7Clabel:S%7C62.107733,-145.541936&markers=size:tiny%7Ccolor:green%7CDelta+Junction,AK&markers=size:mid%7Ccolor:0xFFFF00%7Clabel:C%7CTok,AK&key=AIzaSyCTFUKmdIRSSod5v1oqhIXJOmkOPMsdFp0");
            webBrowser1.Navigate(queryaddress.ToString());

        }

        //Create Standalone SDK class dynamicly.
        public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();

        /********************************************************************************************************************************************
        * Before you refer to this demo,we strongly suggest you read the development manual deeply first.                                           *
        * This part is for demonstrating the communication with your device.There are 3 communication ways: "TCP/IP","Serial Port" and "USB Client".*
        * The communication way which you can use duing to the model of the device.                                                                 *
        * *******************************************************************************************************************************************/
        #region Communication
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private bool bIsConnectedTemp = false;
        private bool bIsConnectedLoaded = false;
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.

        //If your device supports the TCP/IP communications, you can refer to this.
        //when you are using the tcp/ip communication,you can distinguish different devices by their IP address.
        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (txtIP.Text.Trim() == "" || txtPort.Text.Trim() == "")
            {
                MessageBox.Show("IP and Port cannot be null", "Error");
                return;
            }
            int idwErrorCode = 0;

            Cursor = Cursors.WaitCursor;
            if (btnConnect.Text == "DisConnect")
            {
                axCZKEM1.Disconnect();

                this.axCZKEM1.OnFinger -= new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                this.axCZKEM1.OnVerify -= new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                this.axCZKEM1.OnAttTransactionEx -= new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                this.axCZKEM1.OnFingerFeature -= new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                this.axCZKEM1.OnEnrollFingerEx -= new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                this.axCZKEM1.OnDeleteTemplate -= new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                this.axCZKEM1.OnNewUser -= new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                this.axCZKEM1.OnHIDNum -= new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                this.axCZKEM1.OnAlarm -= new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                this.axCZKEM1.OnDoor -= new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                this.axCZKEM1.OnWriteCard -= new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                this.axCZKEM1.OnEmptyCard -= new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);

                bIsConnected = false;
                btnConnect.Text = "Connect";
                lblState.Text = "Current State:DisConnected";
                lbRTShow.Items.Insert(0, "Stopped at " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
                lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        
                Cursor = Cursors.Default;
                return;
            }

            bIsConnected = axCZKEM1.Connect_Net(txtIP.Text, Convert.ToInt32(txtPort.Text));
            if (bIsConnected == true)
            {
               

                btnConnect.Text = "DisConnect";
                btnConnect.Refresh();
                lblState.Text = "Current State:Connected";
                lbRTShow.Items.Insert(0, "Starting at " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
                lbRTShow.Items.Insert(0, "Waiting finger..." );
               


                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                if (axCZKEM1.RegEvent(iMachineNumber, 65535))//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                {
                    this.axCZKEM1.OnFinger += new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                    this.axCZKEM1.OnVerify += new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                    this.axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                    this.axCZKEM1.OnFingerFeature += new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                    this.axCZKEM1.OnEnrollFingerEx += new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                    this.axCZKEM1.OnDeleteTemplate += new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                    this.axCZKEM1.OnNewUser += new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                    this.axCZKEM1.OnHIDNum += new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                    this.axCZKEM1.OnAlarm += new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                    this.axCZKEM1.OnDoor += new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                    this.axCZKEM1.OnWriteCard += new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                    this.axCZKEM1.OnEmptyCard += new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);
                }
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            Cursor = Cursors.Default;
        }

        //If your device supports the SerialPort communications, you can refer to this.
        private void btnRsConnect_Click(object sender, EventArgs e)
        {
            if (cbPort.Text.Trim() == "" || cbBaudRate.Text.Trim() == "" || txtMachineSN.Text.Trim() == "")
            {
                MessageBox.Show("Port,BaudRate and MachineSN cannot be null", "Error");
                return;
            }
            int idwErrorCode = 0;
            //accept serialport number from string like "COMi"
            int iPort;
            string sPort = cbPort.Text.Trim();
            for (iPort = 1; iPort < 10; iPort++)
            {
                if (sPort.IndexOf(iPort.ToString()) > -1)
                {
                    break;
                }
            }

            Cursor = Cursors.WaitCursor;
            if (btnRsConnect.Text == "Disconnect")
            {
                axCZKEM1.Disconnect();

                this.axCZKEM1.OnFinger -= new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                this.axCZKEM1.OnVerify -= new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                this.axCZKEM1.OnAttTransactionEx -= new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                this.axCZKEM1.OnFingerFeature -= new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                this.axCZKEM1.OnEnrollFingerEx -= new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                this.axCZKEM1.OnDeleteTemplate -= new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                this.axCZKEM1.OnNewUser -= new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                this.axCZKEM1.OnHIDNum -= new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                this.axCZKEM1.OnAlarm -= new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                this.axCZKEM1.OnDoor -= new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                this.axCZKEM1.OnWriteCard -= new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                this.axCZKEM1.OnEmptyCard -= new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);

                bIsConnected = false;
                btnRsConnect.Text = "Connect";
                btnRsConnect.Refresh();
                lblState.Text = "Current State:Disconnected";
                Cursor = Cursors.Default;
                return;
            }

            iMachineNumber = Convert.ToInt32(txtMachineSN.Text.Trim());//when you are using the serial port communication,you can distinguish different devices by their serial port number.
            bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, Convert.ToInt32(cbBaudRate.Text.Trim()));

            if (bIsConnected == true)
            {
                btnRsConnect.Text = "Disconnect";
                btnRsConnect.Refresh();
                lblState.Text = "Current State:Connected";

                if (axCZKEM1.RegEvent(iMachineNumber, 65535))//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                {
                    this.axCZKEM1.OnFinger += new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                    this.axCZKEM1.OnVerify += new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                    this.axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                    this.axCZKEM1.OnFingerFeature += new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                    this.axCZKEM1.OnEnrollFingerEx += new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                    this.axCZKEM1.OnDeleteTemplate += new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                    this.axCZKEM1.OnNewUser += new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                    this.axCZKEM1.OnHIDNum += new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                    this.axCZKEM1.OnAlarm += new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                    this.axCZKEM1.OnDoor += new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                    this.axCZKEM1.OnWriteCard += new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                    this.axCZKEM1.OnEmptyCard += new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);
                }
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }

            Cursor = Cursors.Default;
        }

        //If your device supports the USBCLient, you can refer to this.
        //Not all series devices can support this kind of connection.Please make sure your device supports USBClient.
        //Connect the device via the virtual serial port created by USBClient
        private void btnUSBConnect_Click(object sender, EventArgs e)
        {
            int idwErrorCode = 0;

            Cursor = Cursors.WaitCursor;

            if (btnUSBConnect.Text == "Disconnect")
            {
                axCZKEM1.Disconnect();

                this.axCZKEM1.OnFinger -= new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                this.axCZKEM1.OnVerify -= new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                this.axCZKEM1.OnAttTransactionEx -= new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                this.axCZKEM1.OnFingerFeature -= new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                this.axCZKEM1.OnEnrollFingerEx -= new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                this.axCZKEM1.OnDeleteTemplate -= new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                this.axCZKEM1.OnNewUser -= new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                this.axCZKEM1.OnHIDNum -= new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                this.axCZKEM1.OnAlarm -= new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                this.axCZKEM1.OnDoor -= new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                this.axCZKEM1.OnWriteCard -= new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                this.axCZKEM1.OnEmptyCard -= new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);

                bIsConnected = false;
                btnUSBConnect.Text = "Connect";
                btnUSBConnect.Refresh();
                lblState.Text = "Current State:Disconnected";
                Cursor = Cursors.Default;
                return;
            }

            SearchforUSBCom usbcom = new SearchforUSBCom();
            string sCom = "";
            bool bSearch = usbcom.SearchforCom(ref sCom);//modify by Darcy on Nov.26 2009
            if (bSearch == false)//modify by Darcy on Nov.26 2009
            {
                MessageBox.Show("Can not find the virtual serial port that can be used", "Error");
                Cursor = Cursors.Default;
                return;
            }

            int iPort;
            for (iPort = 1; iPort < 10; iPort++)
            {
                if (sCom.IndexOf(iPort.ToString()) > -1)
                {
                    break;
                }
            }

            iMachineNumber = Convert.ToInt32(txtMachineSN2.Text.Trim());
            if (iMachineNumber == 0 || iMachineNumber > 255)
            {
                MessageBox.Show("The Machine Number is invalid!", "Error");
                Cursor = Cursors.Default;
                return;
            }

            int iBaudRate = 115200;//115200 is one possible baudrate value(its value cannot be 0)
            bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, iBaudRate);

            if (bIsConnected == true)
            {
                btnUSBConnect.Text = "Disconnect";
                btnUSBConnect.Refresh();
                lblState.Text = "Current State:Connected";
                if (axCZKEM1.RegEvent(iMachineNumber, 65535))//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                {
                    this.axCZKEM1.OnFinger += new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                    this.axCZKEM1.OnVerify += new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                    this.axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                    this.axCZKEM1.OnFingerFeature += new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                    this.axCZKEM1.OnEnrollFingerEx += new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                    this.axCZKEM1.OnDeleteTemplate += new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                    this.axCZKEM1.OnNewUser += new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                    this.axCZKEM1.OnHIDNum += new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                    this.axCZKEM1.OnAlarm += new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                    this.axCZKEM1.OnDoor += new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                    this.axCZKEM1.OnWriteCard += new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                    this.axCZKEM1.OnEmptyCard += new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);
                }
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }

            Cursor = Cursors.Default;
        }

        #endregion

        /*************************************************************************************************
        * Before you refer to this demo,we strongly suggest you read the development manual deeply first.*
        * This part is for demonstrating the RealTime Events that triggered  by your operations          *
        **************************************************************************************************/
        #region RealTime Events

        //When you place your finger on sensor of the device,this event will be triggered
        private void axCZKEM1_OnFinger()
        {
            lbRTShow.Items.Insert(0, "OnFinger Has been Triggered");
        }

        //After you have placed your finger on the sensor(or swipe your card to the device),this event will be triggered.
        //If you passes the verification,the returned value userid will be the user enrollnumber,or else the value will be -1;
        private void axCZKEM1_OnVerify(int iUserID)
        {
            lbRTShow.Items.Insert(0,"OnVerify Has been Triggered,Verifying...");
            if (iUserID != -1)
            {
                //lbRTShow.Items.Insert(0,"Verified OK,the UserID is " + iUserID.ToString());
                lbRTShow.Items.Insert(0, "Verified OK");
            }
            else
            {
                lbRTShow.Items.Insert(0, "Verified Failed... ");
                lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                lbRTShow.Items.Insert(0, "Waiting finger... ");
            }
        }

        //If your fingerprint(or your card) passes the verification,this event will be triggered
        private void axCZKEM1_OnAttTransactionEx(string sEnrollNumber, int iIsInValid, int iAttState, int iVerifyMethod, int iYear, int iMonth, int iDay, int iHour, int iMinute, int iSecond, int iWorkCode)
        {
            string inOut = methodAbsen[iAttState];
            lbRTShow.Items.Insert(0,"UserID: " + sEnrollNumber);
            lbRTShow.Items.Insert(0, "Mencoba Absen " + inOut);

            switch (iAttState)
            {
                case 0:
                    CheckINExist(sEnrollNumber, iAttState);
                    break;
                case 1:
                    CheckOUTExist(sEnrollNumber, iAttState);
                    break;
                default:
                    AksesDitolak();
                    break;
            }
        }

        private void PostAbsen (string idEmployee, int isState)
        {
            string inOut = methodAbsen[isState];
            try
            {
                System.Net.WebClient oWeb = new WebClientWithTimeout();
                oWeb.Proxy = System.Net.WebRequest.DefaultWebProxy;
                oWeb.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                oWeb.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] bytArguments = System.Text.Encoding.ASCII.GetBytes("Latitude=" + txtLat.Text + "&Longitude=" + txtLong.Text + "&EmployeeID=" + idEmployee + "&InOut=" + inOut);
                byte[] bytRetData = oWeb.UploadData(url + "attendances/PostAttendanceInOutFingerWorkingPatternWeekly", "POST", bytArguments);
                string response = System.Text.Encoding.ASCII.GetString(bytRetData);
                string temp = response;

                RootObject respon = new RootObject();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                respon = (RootObject)jss.Deserialize(temp, typeof(RootObject));
                if (respon.errCode == 0)
                {
                    if (respon.data.status == null)
                    {
                        // aw aw
                        axCZKEM1.PlayVoiceByIndex(10);
                        lbRTShow.Items.Insert(0, "Absen BERHASIL pada " + respon.data.dateCreated + " dengan ID " + respon.data.attendanceID.ToString() + " a.n " + respon.data.employeeName);
                        lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                        lbRTShow.Items.Insert(0, "Waiting finger... ");

                    }
                    else
                    {
                        TidakAdaJamKerja();
                    }
 
                }
                else
                {
                    GagalAbsen();
                }
            }
            catch (Exception e)
            {
                 GagalAbsen();
            }
        }

        private void CheckINExist(string idEmployee, int isState)
        {
            try
            {
                System.Net.WebClient oWeb = new WebClientWithTimeout();
                oWeb.Proxy = System.Net.WebRequest.DefaultWebProxy;
                oWeb.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string response = oWeb.DownloadString(url + "Attendances/CheckAlreadyINFinger?employeeID=" + idEmployee);
                string temp = response;

                RootObjectCheckINOUT respon = new RootObjectCheckINOUT();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                respon = (RootObjectCheckINOUT)jss.Deserialize(temp, typeof(RootObjectCheckINOUT));

                if (respon.data.status.Equals("Success"))
                {
                    PostAbsen(idEmployee, isState);
                }
                else
                {
                    AbsenSudahAda();
                }
            }
            catch (Exception e)
            { 
                GagalAbsen();
            }
        }

        private void CheckOUTExist(string idEmployee, int isState)
        {
            try
            {
                System.Net.WebClient oWeb = new WebClientWithTimeout();
                oWeb.Proxy = System.Net.WebRequest.DefaultWebProxy;
                oWeb.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string response = oWeb.DownloadString(url + "Attendances/CheckAlreadyOUTFinger?employeeID=" + idEmployee);
                string temp = response;

                RootObjectCheckINOUT respon = new RootObjectCheckINOUT();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                respon = (RootObjectCheckINOUT)jss.Deserialize(temp, typeof(RootObjectCheckINOUT));

                if (respon.data.status.Equals("Success"))
                {
                    PostAbsen(idEmployee, isState);
                }
                else
                {
                    if (respon.data.result.Contains("belum"))
                    {
                        BelumAbsenIN();
                    }
                    else
                    {
                        AbsenSudahAda();
                    }
                }
            }
            catch (Exception e)
            {   
                GagalAbsen();
            }
        }

        private void BelumAbsenIN()
        {
            //Nomor ID salah
            axCZKEM1.PlayVoiceByIndex(3);
            lbRTShow.Items.Insert(0, "Belum Absen IN pada " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            lbRTShow.Items.Insert(0, "Waiting finger... ");
        }

        private void GagalAbsen()
        {
            // silahkan coba lagi
            axCZKEM1.PlayVoiceByIndex(4);
            lbRTShow.Items.Insert(0, "Gagal absen pada " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            lbRTShow.Items.Insert(0, "Waiting finger... ");
        }

        private void AksesDitolak()
        {
            // akses ditolak
            axCZKEM1.PlayVoiceByIndex(2);
            lbRTShow.Items.Insert(0, "Akses ditolak pada " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            lbRTShow.Items.Insert(0, "Waiting finger... ");
        }

        private void TidakAdaJamKerja()
        {
            // akses ditolak
            axCZKEM1.PlayVoiceByIndex(2);
            lbRTShow.Items.Insert(0, "Tidak ada jam kerja pada " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            lbRTShow.Items.Insert(0, "Waiting finger... ");
        }

        private void AbsenSudahAda()
        {
            //Nomor ID sudah ada
            axCZKEM1.PlayVoiceByIndex(5);
            lbRTShow.Items.Insert(0, "Absen sudah ada pada " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            lbRTShow.Items.Insert(0, "Waiting finger... ");
        }

        public class WebClientWithTimeout : WebClient
        {
            protected override WebRequest GetWebRequest(Uri address)
            {
                WebRequest wr = base.GetWebRequest(address);
                wr.Timeout = 3000; // timeout in milliseconds (ms)
                return wr;
            }
        }

        //Generate from http://json2csharp.com/#
        public class Data
        {
            public int attendanceID { get; set; }
            public int employeeID { get; set; }
            public DateTime dateTime { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
            public DateTime dateCreated { get; set; }
            public double lateTimeInMinute { get; set; }
            public bool isLate { get; set; }
            public bool isMobileApp { get; set; }
            public int organizationID { get; set; }
            public object employeeName { get; set; }
            public object employee { get; set; }
            public string status { get; set; }
            public string result { get; set; }

        }

        public class RootObject
        {
            public int errCode { get; set; }
            public string errMessage { get; set; }
            public Data data { get; set; }
        }

        //class jam server
        public class RootObjectJamServer
        {
            public int errCode { get; set; }
            public string errMessage { get; set; }
            public DateTime data { get; set; }
        }


     

        //When you have enrolled your finger,this event will be triggered and return the quality of the fingerprint you have enrolled
        private void axCZKEM1_OnFingerFeature(int iScore)
        {
            if (iScore < 0)
            {
                lbRTShow.Items.Insert(0,"The quality of your fingerprint is poor");
            }
            else
            {
                lbRTShow.Items.Insert(0,"RTEvent OnFingerFeature Has been Triggered...Score:　" + iScore.ToString());
            }
        }

        //When you are enrolling your finger,this event will be triggered.
        private void axCZKEM1_OnEnrollFingerEx(string sEnrollNumber, int iFingerIndex, int iActionResult, int iTemplateLength)
        {
            if (iActionResult == 0)
            {
                lbRTShow.Items.Insert(0,"RTEvent OnEnrollFigerEx Has been Triggered....");
                lbRTShow.Items.Insert(0, ".....UserID: " + sEnrollNumber + " Index: " + iFingerIndex.ToString() + " tmpLen: " + iTemplateLength.ToString());
            }
            else
            {
                lbRTShow.Items.Insert(0,"RTEvent OnEnrollFigerEx Has been Triggered Error,actionResult=" + iActionResult.ToString());
            }
        }

        //When you have deleted one one fingerprint template,this event will be triggered.
        private void axCZKEM1_OnDeleteTemplate(int iEnrollNumber, int iFingerIndex)
        {
            lbRTShow.Items.Insert(0,"RTEvent OnDeleteTemplate Has been Triggered...");
            lbRTShow.Items.Insert(0,"...UserID=" + iEnrollNumber.ToString() + " FingerIndex=" + iFingerIndex.ToString());
        }

        //When you have enrolled a new user,this event will be triggered.
        private void axCZKEM1_OnNewUser(int iEnrollNumber)
        {
            lbRTShow.Items.Insert(0,"RTEvent OnNewUser Has been Triggered...");
            lbRTShow.Items.Insert(0,"...NewUserID=" + iEnrollNumber.ToString());
        }

        //When you swipe a card to the device, this event will be triggered to show you the card number.
        private void axCZKEM1_OnHIDNum(int iCardNumber)
        {
            lbRTShow.Items.Insert(0,"RTEvent OnHIDNum Has been Triggered...");
            lbRTShow.Items.Insert(0,"...Cardnumber=" + iCardNumber.ToString());
        }

        //When the dismantling machine or duress alarm occurs, trigger this event.
        private void axCZKEM1_OnAlarm(int iAlarmType, int iEnrollNumber, int iVerified)
        {
            lbRTShow.Items.Insert(0,"RTEvnet OnAlarm Has been Triggered...");
            lbRTShow.Items.Insert(0,"...AlarmType=" + iAlarmType.ToString());
            lbRTShow.Items.Insert(0,"...EnrollNumber=" + iEnrollNumber.ToString());
            lbRTShow.Items.Insert(0,"...Verified=" + iVerified.ToString());
        }

        //Door sensor event
        private void axCZKEM1_OnDoor(int iEventType)
        {
            lbRTShow.Items.Insert(0,"RTEvent Ondoor Has been Triggered...");
            lbRTShow.Items.Insert(0,"...EventType=" + iEventType.ToString());
        }

        //When you have emptyed the Mifare card,this event will be triggered.
        private void axCZKEM1_OnEmptyCard(int iActionResult)
        {
            lbRTShow.Items.Insert(0,"RTEvent OnEmptyCard Has been Triggered...");
            if (iActionResult == 0)
            {
                lbRTShow.Items.Insert(0,"...Empty Mifare Card OK");
            }
            else
            {
                lbRTShow.Items.Insert(0,"...Empty Failed");
            }
        }

        //When you have written into the Mifare card ,this event will be triggered.
        private void axCZKEM1_OnWriteCard(int iEnrollNumber, int iActionResult, int iLength)
        {
            lbRTShow.Items.Insert(0,"RTEvent OnWriteCard Has been Triggered...");
            if (iActionResult == 0)
            {
                lbRTShow.Items.Insert(0,"...Write Mifare Card OK");
                lbRTShow.Items.Insert(0,"...EnrollNumber=" + iEnrollNumber.ToString());
                lbRTShow.Items.Insert(0,"...TmpLength=" + iLength.ToString());
            }
            else
            {
                lbRTShow.Items.Insert(0,"...Write Failed");
            }
        }

        //After function GetRTLog() is called ,RealTime Events will be triggered. 
        //When you are using these two functions, it will request data from the device forwardly.
        private void rtTimer_Tick(object sender, EventArgs e)
        {
            if (axCZKEM1.ReadRTLog(iMachineNumber))
            {
                while (axCZKEM1.GetRTLog(iMachineNumber))
                {
                    ;
                }
            }
        }

        #endregion

        private void pictureBox1_Click(object sender, System.EventArgs e)
        {

        }

        private void BtnChangeSetting_Click(object sender, System.EventArgs e)
        {
            if (BtnChangeSetting.Text == "Change Setting")
            {
                if (txtPwd.Text == "user123")
                {
                    btnSave.Visible = true;
                    BtnChangeSetting.Text = "Secure Setting";
                    //txtLat.ReadOnly = false;
                    //txtLong.ReadOnly = false;
                }
                else
                {
                    MessageBox.Show("Password Salah");
                }
            }
            else
            {
                btnSave.Visible = false;
                BtnChangeSetting.Text = "Change Setting";
                //txtLat.ReadOnly = true;
                //txtLong.ReadOnly = true;
            }
            txtPwd.Text = "";
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            //txtLat.ReadOnly = true;
            //txtLong.ReadOnly = true;
        }

        private void label3_Click(object sender, System.EventArgs e)
        {

        }

        private void txtLat_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void Disconnect ()
        {
           // lbRTShow.Items.Insert(0, "Disconnect at " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            axCZKEM1.Disconnect();
            this.axCZKEM1.OnFinger -= new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
            this.axCZKEM1.OnVerify -= new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
            this.axCZKEM1.OnAttTransactionEx -= new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
            this.axCZKEM1.OnFingerFeature -= new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
            this.axCZKEM1.OnEnrollFingerEx -= new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
            this.axCZKEM1.OnDeleteTemplate -= new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
            this.axCZKEM1.OnNewUser -= new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
            this.axCZKEM1.OnHIDNum -= new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
            this.axCZKEM1.OnAlarm -= new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
            this.axCZKEM1.OnDoor -= new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
            this.axCZKEM1.OnWriteCard -= new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
            this.axCZKEM1.OnEmptyCard -= new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);
            bIsConnectedTemp = false;

        }

        private void Connect()
        {         
            bIsConnectedTemp = axCZKEM1.Connect_Net(txtIP.Text, Convert.ToInt32(txtPort.Text));
            if (bIsConnectedTemp == true)
            {
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                if (axCZKEM1.RegEvent(iMachineNumber, 65535))//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                {
                    lbRTShow.Items.Insert(0, "Success reconnect at " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
                    lbRTShow.Items.Insert(0, "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                    lbRTShow.Items.Insert(0, "Waiting finger...");

                    this.axCZKEM1.OnFinger += new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                    this.axCZKEM1.OnVerify += new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                    this.axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                    this.axCZKEM1.OnFingerFeature += new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                    this.axCZKEM1.OnEnrollFingerEx += new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                    this.axCZKEM1.OnDeleteTemplate += new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                    this.axCZKEM1.OnNewUser += new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                    this.axCZKEM1.OnHIDNum += new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                    this.axCZKEM1.OnAlarm += new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                    this.axCZKEM1.OnDoor += new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                    this.axCZKEM1.OnWriteCard += new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                    this.axCZKEM1.OnEmptyCard += new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);
                }
            }
            else
            {
                lbRTShow.Items.Insert(0, "Failed reconnect at " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            }
        }

        private void trJamServer_Tick(object sender, System.EventArgs e)
        {
            //add 1 detik
            String minutess,secondss;

            minutess = _start.ToString("mm");
            secondss = _start.ToString("ss");

            if (minutess == "45" && secondss == "45")
            {
                getServerTime();
                {
                     _start = _tempStart;
                    // MessageBox.Show("Sukses update");
                }
            }

            _start = _start.AddSeconds(1);
            TimeSpan duration =  _start.TimeOfDay;
            label8.Text = duration.ToString(@"hh\:mm\:ss"); ;           
        }

        



        private Boolean  getServerTime()
        {
            try
            {
                System.Net.WebClient oWeb = new System.Net.WebClient();
                oWeb.Proxy = System.Net.WebRequest.DefaultWebProxy;
                oWeb.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string response = oWeb.DownloadString("http://hras.warnawarni.co.id/api/getservertime");
                string temp = response;

                RootObjectJamServer respon = new RootObjectJamServer();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                respon = (RootObjectJamServer)jss.Deserialize(temp, typeof(RootObjectJamServer));

                _tempStart = respon.data;
                lblStatus.Text = "Connected";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                label9.Text = respon.data.ToString("dddd,dd MMMM yyyy");
                return true;           
            }
            catch (Exception e)
            {
                lblStatus.Text = "Not Connected";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return false;
            }
        }

        private void label8_Click(object sender, System.EventArgs e)
        {

        }

        private void label4_Click(object sender, System.EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, System.EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, System.EventArgs e)
        {

        }

        private void label11_Click(object sender, System.EventArgs e)
        {

        }

        private void txtLat_Click(object sender, EventArgs e)
        {

        }

        private void RTEventsMain_Load(object sender, EventArgs e)
        {

        }

        private void trReconnect_Tick(object sender, EventArgs e)
        {
         
            if (bIsConnected == true)
            {           
                    Disconnect();
                    Connect();            
            }

        }

        private void label11_Click_1(object sender, EventArgs e)
        {

        }

        private void tmrPing_Tick(object sender, System.EventArgs e)
        {

            //try
            //{

            //    Ping ping = new Ping();
            //    PingReply pingresult = ping.Send(txtIP.Text);
            //    if (pingresult.Status.ToString() == "Success")
            //    {
            //        lblStatusDevice.Text = "Connected";
            //        lblStatusDevice.ForeColor = System.Drawing.Color.Green;
            //    }
            //    else
            //    {
            //        lblStatusDevice.Text = "Not Connected";
            //        lblStatusDevice.ForeColor = System.Drawing.Color.Red;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    lblStatusDevice.Text = "Not Connected";
            //    lblStatusDevice.ForeColor = System.Drawing.Color.Red;
            //}
        }
    }

    //generateAddress
    public class PlusCode
    {
        public string compound_code { get; set; }
        public string global_code { get; set; }
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast2 northeast { get; set; }
        public Southwest2 southwest { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class PlusCode2
    {
        public string compound_code { get; set; }
        public string global_code { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
        public PlusCode2 plus_code { get; set; }
    }

    public class RootObjectAddress
    {
        public PlusCode plus_code { get; set; }
        public List<Result> results { get; set; }
        public string status { get; set; }
    }


    public class DataCheckINOUT
    {
        public string status { get; set; }
        public string result { get; set; }
    }

    public class RootObjectCheckINOUT
    {
        public int errCode { get; set; }
        public string errMessage { get; set; }
        public DataCheckINOUT data { get; set; }
    }




} 